namespace TimeTrees.Core.WriteTools
{
    public static class GenerateTestCsvFile
    {
        public static string[] GeneratePeopleFileCsv()
        {
            string[] strings = new []
            {
                "1;Имя 1;0;0;2000-06-05",
                "2;Имя 2;1;0;1950-01-10;2010-01-01"
            };
            return strings;
        }

        public static string[] GenerateTimeLineFileCsv()
        {
            string[] strings = new[]
            {
                "1950;событие 1 бла-бла-бла",
                "1991-06-01;какое-то событие 2",
                "2000-01-01;наступил миллениум, ура-ура-ура"
            };
            return strings;
        }
    }
}