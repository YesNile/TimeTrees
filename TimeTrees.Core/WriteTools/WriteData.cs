using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Newtonsoft.Json;
using TimeTrees.Core.Models;
using TimeTrees.Core.ReadTools;

namespace TimeTrees.Core.WriteTools
{
    public static class WriteData
    {
        public static void WritePeopleInFile(List<Person> persons, Format format)
        {
            string path = "../../../../files/people.csv";
            StringBuilder fileText = new StringBuilder();
            if (format == Format.Csv)
            {
                foreach (var person in persons)
                {
                    int first = person.FirstParent?.Id ?? 0;
                    int second = person.SecondParent?.Id ?? 0;
                    if (person.DeathDate == null)
                    {
                        fileText.Append($"{person.Id};{person.Name};{first};{second};{person.BirthDate.ToString("yyyy-MM-dd")}\n");

                    }
                    else
                    {
                        string death = Convert.ToDateTime(person.DeathDate).ToString("yyyy-MM-dd");
                        fileText.Append($"{person.Id};{person.Name};{first};{second};{person.BirthDate.ToString("yyyy-MM-dd")};{death}\n");
                    }
                }
                File.WriteAllText(path,fileText.ToString());
            }
            else
                WritePeopleInJsonFile(persons);
        }

        public static void WriteTimeLineEventInFile(List<TimelineEvent> timelineEvents, Format format)
        {
            string path = "../../../../files/timeline.csv";
            if (format == Format.Csv)
            {
                StringBuilder fileText = new StringBuilder();
                foreach (var timeline in timelineEvents)
                {
                    fileText.Append($"{timeline.Id};{timeline.Date.ToString("yyyy-MM-dd")};{timeline.Event}\n");
                }
                File.WriteAllText(path,fileText.ToString());
            }
            else WriteTimeLineEventInJsonFile(timelineEvents);
        }
        public static void WriteTimeLineEventInJsonFile(List<TimelineEvent> timelineEvents)
        {
            string fileText = JsonConvert.SerializeObject(timelineEvents, Formatting.Indented);
            File.WriteAllText("../../../../files/timeline.json",fileText);
        }

        public static void WritePeopleInJsonFile(List<Person> persons)
        {
            string fileText = JsonConvert.SerializeObject(persons, Formatting.Indented);
            File.WriteAllText("../../../../files/people.json",fileText);
        }
    }
}