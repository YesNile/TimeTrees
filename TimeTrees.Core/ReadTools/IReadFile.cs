using System.Collections.Generic;
using TimeTrees.Core.Models;

namespace TimeTrees.Core.ReadTools
{
    public interface IReadFile
    {
        List<Person> ReadPersonFile();
        List<TimelineEvent> ReadTimelineEventsFile();
    }
}