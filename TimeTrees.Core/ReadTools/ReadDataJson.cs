using System.Collections.Generic;
using System.IO;
using System.Linq;
using Newtonsoft.Json;
using TimeTrees.Core.Models;

namespace TimeTrees.Core.ReadTools
{
    public class ReadDataJson : IReadFile
    {
        public List<Person> ReadPersonFile()
        {
            string path = "../../../../files/people.json";
            CheckFileExistOrGenerateIt(path);
            string peopleLine = File.ReadAllText(path);
            List<Person> personList = 
                JsonConvert.DeserializeObject<Person[]>(peopleLine).ToList();
            
            return personList;
        }

        public List<TimelineEvent> ReadTimelineEventsFile()
        {
            string path = "../../../../files/timeline.json";
            CheckFileExistOrGenerateIt(path);
            string timelineLineFile = File.ReadAllText(path);
            List<TimelineEvent> timelineList = 
                JsonConvert.DeserializeObject<TimelineEvent[]>(timelineLineFile).ToList();
            
            return timelineList;
        }
        public static void CheckFileExistOrGenerateIt(string path)
        {
            if (!File.Exists(path))
                File.WriteAllText(path, "[]");
        }
    }
}