using System;
using System.Collections.Generic;
using System.Linq;
using TimeTrees.Core.Models;

namespace TimeTrees.Core.WorkWithDateTools
{
    public class LeapYear
    {
        public readonly List<Person> Persons;
        
        public LeapYear(List<Person> persons)
        {
            Persons = SetListPersonBornInLeapYear(persons);
        }
        private static List<Person> SetListPersonBornInLeapYear(List<Person> personList) => personList.Where(x => DateTime.IsLeapYear(x.BirthDate.Year) && Check20Years(DateTime.Now, x.BirthDate)).ToList();

        private static bool Check20Years(DateTime dateNow, DateTime personBirthDate) =>
            (dateNow.Year - personBirthDate.Year < 20) || ((dateNow.Year - personBirthDate.Year == 20) &&
                                                           (dateNow.Month - personBirthDate.Month <= 0) &&
                                                           (dateNow.Day - personBirthDate.Day <= 0));
    }
}