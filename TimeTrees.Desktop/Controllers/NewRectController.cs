﻿using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Shapes;
using TimeTrees.Core.Models;
using TimeTrees.DesktopGui.Model;
using TimeTrees.DesktopGui.States;

namespace TimeTrees.DesktopGui.Controllers
{
    sealed internal class NewRectController : Controller
    {
        private Rectangle _newRect;

        public NewRectController(Tools tools) : base(tools)
        {
            if (Tools.MainWindow.Data.CheckPersonExist())
            {
                tools.Canvas.MouseMove += OnMouseMove;
                tools.Canvas.MouseDown += OnMouseDown;
            }
        }

        private void OnMouseMove(object sender, MouseEventArgs e)
        {
            var mouseX = e.GetPosition(Tools.Canvas).X;
            var mouseY = e.GetPosition(Tools.Canvas).Y;
            var additionalInfo = GetHoveredShapesInfo();

            var additionalNameInfo = GetHoveredShapesName();
            Tools.StatusBar.Update(State.Edit, e.GetPosition(Tools.Canvas), additionalInfo, additionalNameInfo);

            if (_newRect == null)
            {
                var person =Tools.MainWindow.Data.GetPerson();
                _newRect = new Rectangle();
                _newRect.Width = person.Name.Length*15>50? person.Name.Length*15 : 50;
                _newRect.Height = 50;
                _newRect.Stroke = Brushes.Black;
                _newRect.StrokeThickness = 1.5;
                _newRect.Fill = Brushes.White;
                _newRect.DataContext = person;
                var menu = new ContextMenu();
                var mi = new MenuItem();
                mi.Height = 25;
                mi.Header = "Удалить";
                mi.Click += base.Delete;
                menu.Items.Add(mi);
                _newRect.ContextMenu = menu;
                
                Tools.Canvas.Children.Add(_newRect);
                Canvas.SetZIndex(_newRect, 2);
            }

            if (mouseX <= 3 | mouseX >= Tools.Canvas.ActualWidth - 3 | mouseY <= 3 | mouseY >= Tools.Canvas.ActualHeight - 3)
            {
                Tools.Canvas.Children.Remove(_newRect);
                _newRect = null;
            }

            if (_newRect != null)
            {
                Canvas.SetTop(_newRect, mouseY - 25);
                Canvas.SetLeft(_newRect, mouseX - 25);
            }
        }

        private void OnMouseDown(object sender, MouseButtonEventArgs e)
        {
            Tools.RectangleDao.Add(new RectangleModel(_newRect));
            Tools.ShapesDao.AddRectangle(_newRect);
            var rect = Tools.RectangleDao.Get(((Person) _newRect.DataContext).Id);
            Tools.Canvas.Children.Add(rect.RectangleName);
            Canvas.SetZIndex(rect.RectangleName,4);
            Canvas.SetTop(rect.RectangleName,Canvas.GetTop(_newRect));
            Canvas.SetLeft(rect.RectangleName,Canvas.GetLeft(_newRect));
            _newRect = null;
            Tools.MainWindow.Data.SetNullPerson();
            Tools.MainWindow.DisableTools();
        }

        private void Reload()
        {
            Tools.Canvas.MouseDown -= OnMouseDown;
            Tools.Canvas.MouseDown += OnMouseDown;
        }

        public override void Unload()
        {
            Tools.Canvas.MouseMove -= OnMouseMove;
            Tools.Canvas.MouseDown -= OnMouseDown;
            Dispose();
        }
    }
}
