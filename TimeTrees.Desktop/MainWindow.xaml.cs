﻿using System.Drawing;
using System.Windows;
using TimeTrees.DesktopGui.Additional_tools;
using TimeTrees.DesktopGui.Controllers;
using TimeTrees.DesktopGui.DAO;
using TimeTrees.DesktopGui.Model;
using TimeTrees.DesktopGui.Windows;
using System.Windows.Media;
using TimeTrees.DesktopGui.States;

namespace TimeTrees.DesktopGui
{
    public partial class MainWindow : Window
    {
        private readonly Tools _tools;
        private Controller _currentController;
        public ViewRectangleInfo ViewRectangleInfo { get; set; }
        public ConnectTypeChoice ConnectType { get; set; }
        public InputData Data { get; set; }

        public Theme CurrentTheme { get; private set; }
        public MainWindow()
        {
            CurrentTheme = Theme.Black;
            InitializeComponent();

            StatusBar sb = new (lblCoordinates, lblState, lblHovering, personName);
            ShapesDao dao = new();
            RectangleDao rectangleDao = new RectangleDao();
            
            _tools = new Tools(this, canvas, sb, dao, rectangleDao);
            _currentController = new EditController(_tools);
            
            DrawSavedData.DrawSavedRect(_currentController, _tools);
        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            Data = new InputData(CurrentTheme);
            Data.ShowDialog();
            if (_currentController != null) _currentController.Unload();
            _currentController = new NewRectController(_tools);
        }

        private void btnConnection_Click(object sender, RoutedEventArgs e)
        {
            if (_currentController != null) _currentController.Unload();
            _currentController = new NewConnectionController(_tools);
        }
        
        private void BtnEdit_OnClick(object sender, RoutedEventArgs e)
        {
            if (_currentController != null) _currentController.Unload();
            _currentController = new EditController(_tools);
        }

        public void DisableTools()
        {
            if (_currentController != null) _currentController.Unload();
            _currentController = new EditController(_tools);
        }

        private void BtnSave_OnClick(object sender, RoutedEventArgs e)
        {
            _tools.RectangleDao.Save(canvas);
            MessageBox.Show("Сохранено!");
        }

        private void btnSwitchTheme(object sender, RoutedEventArgs e)
        {
            if (CurrentTheme == Theme.Black)
            {
                _tools.ShapesDao.ChangeConnectionColor(Brushes.Black);

                var canvasColor = new BrushConverter().ConvertFromString("#ffffff") as SolidColorBrush;
                var sbCanvasBarColor = new BrushConverter().ConvertFromString("#f5f5f5") as SolidColorBrush;
                var btnBackgroundColor = new BrushConverter().ConvertFromString("#b0b0b0") as SolidColorBrush;
                var foregroundColor = new BrushConverter().ConvertFromString("#000000") as SolidColorBrush;

                CurrentTheme = Theme.White;
                btnTheme.Content = "Black";

                canvas.Background = canvasColor;
                sbCanvas.Background = sbCanvasBarColor;
                upBar.Background = sbCanvasBarColor;

                btnAdd.Background = btnBackgroundColor;
                btnAdd.Foreground = foregroundColor;
                btnConnect.Background = btnBackgroundColor;
                btnConnect.Foreground = foregroundColor;
                btnEdit.Background = btnBackgroundColor;
                btnEdit.Foreground = foregroundColor;
                btnTheme.Background = btnBackgroundColor;
                btnTheme.Foreground = foregroundColor;
                btnSave.Background = btnBackgroundColor;
                btnSave.Foreground = foregroundColor;

                lblCoordinates.Foreground = foregroundColor;
                lblHovering.Foreground = foregroundColor;
                lblState.Foreground = foregroundColor;
                lblState.Foreground = foregroundColor;
                lblState.Foreground = foregroundColor;
                personName.Foreground = foregroundColor;
            }
            else
            {
                _tools.ShapesDao.ChangeConnectionColor(Brushes.White);
                
                var canvasColor = new BrushConverter().ConvertFromString("#1a1a1a") as SolidColorBrush;
                var sbCanvasBarColor = new BrushConverter().ConvertFromString("#3d3d3d") as SolidColorBrush;
                var btnBackgroundColor = new BrushConverter().ConvertFromString("#2e2e2e") as SolidColorBrush;
                var foregroundColor = new BrushConverter().ConvertFromString("#ffffff") as SolidColorBrush;

                CurrentTheme = Theme.Black;
                btnTheme.Content = "White";

                canvas.Background = canvasColor;
                sbCanvas.Background = sbCanvasBarColor;
                upBar.Background = sbCanvasBarColor;

                btnAdd.Background = btnBackgroundColor;
                btnAdd.Foreground = foregroundColor;
                btnConnect.Background = btnBackgroundColor;
                btnConnect.Foreground = foregroundColor;
                btnEdit.Background = btnBackgroundColor;
                btnEdit.Foreground = foregroundColor;
                btnTheme.Background = btnBackgroundColor;
                btnTheme.Foreground = foregroundColor;
                btnSave.Background = btnBackgroundColor;
                btnSave.Foreground = foregroundColor;

                lblCoordinates.Foreground = foregroundColor;
                lblHovering.Foreground = foregroundColor;
                lblState.Foreground = foregroundColor;
                lblState.Foreground = foregroundColor;
                lblState.Foreground = foregroundColor;
                personName.Foreground = foregroundColor;
            }
        }
    }
}