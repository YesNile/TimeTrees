using System;
using System.Windows;
using TimeTrees.Core.WorkWithDateTools;

namespace TimeTrees.DesktopGui.Additional_tools
{
    public static class CheckInputData
    {
        public static bool CheckCorrectlyInput(string name, string birthDate, string deathDate)
        {
            
            
            if (name == string.Empty)
            {
                MessageBox.Show("Введите \"Имя\" ");
                return false;
            }

            if (birthDate == string.Empty)
            {
                MessageBox.Show("Введите \"Дату рождения\" ");
                return false;
            }
            else
            {
                try
                {
                    DateHelper.ParseDate(birthDate);
                }
                catch (Exception e)
                {
                    MessageBox.Show($"{e.Message}\n Неправильный формат даты (\"ГГГГ-ММ-ДД\")");
                    return false;
                }
            }
            if (deathDate != string.Empty)
            {
                try
                {
                    DateHelper.ParseDate(deathDate);
                }
                catch (Exception e)
                {
                    MessageBox.Show($"{e.Message}\n Неправильный формат даты (\"ГГГГ-ММ-ДД\")");
                    return false;
                }
            }
            
            return true;
        }
    }
}