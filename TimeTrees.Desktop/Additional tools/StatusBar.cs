﻿using System.Windows;
using System.Windows.Controls;
using TimeTrees.DesktopGui.States;

namespace TimeTrees.DesktopGui.Additional_tools
{
    internal class StatusBar
    {
        private readonly Label _lblCoordinates;
        private readonly Label _lblCurrentState;
        private readonly Label _lblIsHover;
        private readonly Label _personName;

        public StatusBar(Label lblCoordinates, Label lblCurrentState, Label lblIsHover, Label personName)
        {
            _lblCoordinates = lblCoordinates;
            _lblCurrentState = lblCurrentState;
            _lblIsHover = lblIsHover;
            _personName = personName;
        }

        public void Update(State currentState, Point? point, string additionalInfo = null,string additionalNameInfo = null)
        {
            UpdateCurrentState(currentState);
            UpdateCoordinates(point);
            _personName.Content=additionalNameInfo;
            _lblIsHover.Content = additionalInfo;
        }

        private void UpdateCurrentState(State currentState) => _lblCurrentState.Content = currentState.ToString();

        private void UpdateCoordinates(Point? point)
        {
            if (point.HasValue)
            {
                _lblCoordinates.Content = $"X:{point.Value.X.ToString("000")} Y:{point.Value.Y.ToString("000")} ";
            }
        }
    }
}
