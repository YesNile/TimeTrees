using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;
using TimeTrees.DesktopGui.Controllers;
using TimeTrees.DesktopGui.DAO;
using TimeTrees.DesktopGui.Model;
using TimeTrees.DesktopGui.States;

namespace TimeTrees.DesktopGui.Additional_tools
{
    static internal class DrawSavedData
    {
        public static void DrawSavedRect(Controller controller, Tools args)
        {
            foreach (var model in args.RectangleDao.GetAll())
            {
                var rect = model.Rectangle;
                rect.Width = model.RectangleName.Text.Length*5>50? model.RectangleName.Text.Length*5 : 50;
                rect.Height = 50;
                
                var menu = new ContextMenu();
                var mi = new MenuItem();
                mi.Height = 25;
                mi.Header = "Удалить";
                mi.Click += controller.Delete;
                menu.Items.Add(mi);
                rect.ContextMenu = menu;
                
                args.Canvas.Children.Add(rect);
                Canvas.SetTop(rect, model.CanvasPositionY);
                Canvas.SetLeft(rect, model.CanvasPositionX);
                Canvas.SetZIndex(rect, 2);
                args.Canvas.Children.Add(model.RectangleName);
                Canvas.SetLeft(model.RectangleName,model.CanvasPositionX);
                Canvas.SetTop(model.RectangleName,model.CanvasPositionY);
                Canvas.SetZIndex(model.RectangleName,4);
            }
            DrawConnection(controller, args);
        }

        private static void DrawConnection(Controller controller, Tools args)
        {
            foreach (var model in args.RectangleDao.GetAll())
            {
                if (model.Children == null) continue;
                foreach (var child in model.Children)
                {
                    var destination = args.RectangleDao.Get(child);
                    Polyline connection = new Polyline();
                    connection.FillRule = FillRule.Nonzero;
                    connection.Stroke = Brushes.White;
                    connection.StrokeThickness = 2;
                    var menu = new ContextMenu();
                    var mi = new MenuItem();
                    mi.Height = 25;
                    mi.Header = "Удалить";
                    mi.Click += controller.Delete;
                    menu.Items.Add(mi);
                    connection.ContextMenu = menu;
                    
                    connection.UpdatePolyline(model.Rectangle,destination.Rectangle,ConnectionType.Parent);

                    args.Canvas.Children.Add(connection);
                    Canvas.SetZIndex(connection, 1);
                    
                    args.ShapesDao.AddConnection(model.Rectangle,destination.Rectangle, connection, ConnectionType.Parent);
                }
            }

            foreach (var model in args.RectangleDao.GetAll())
            {
                if(model.Spouse == 0) continue;
                
                var destination = args.RectangleDao.Get(model.Spouse);
                Polyline connection = new Polyline();
                connection.FillRule = FillRule.Nonzero;
                connection.Stroke = Brushes.White;
                connection.StrokeThickness = 2;
                var menu = new ContextMenu();
                var mi = new MenuItem();
                mi.Height = 25;
                mi.Header = "Удалить";
                mi.Click += controller.Delete;
                menu.Items.Add(mi);
                connection.ContextMenu = menu;
                
                connection.UpdatePolyline(model.Rectangle, destination.Rectangle, ConnectionType.Spouses);

                if (!args.ShapesDao.CheckRectExist(model.Rectangle, destination.Rectangle))
                {
                    args.ShapesDao.AddConnection(model.Rectangle, destination.Rectangle, connection, ConnectionType.Spouses);
                    args.Canvas.Children.Add(connection);
                    Canvas.SetZIndex(connection, 1);
                }
            }
        }
    }
}