using System.Windows;
using TimeTrees.Core.Models;
using TimeTrees.Core.WorkWithDateTools;
using TimeTrees.DesktopGui.Additional_tools;
using TimeTrees.DesktopGui.States;
using System.Windows.Media;

namespace TimeTrees.DesktopGui.Windows
{
    public partial class InputData
    {
        private Person _newPerson;
        public InputData(Theme theme)
        {
            InitializeComponent();
            SetThemeInputData(theme);
        }

        private void SetThemeInputData(Theme currentTheme)
        {
            if (currentTheme == Theme.Black)
            {
                var backBackgroundColor = new BrushConverter().ConvertFromString("#3d3d3d") as SolidColorBrush;
                var btnBackgroundColor = new BrushConverter().ConvertFromString("#2e2e2e") as SolidColorBrush;
                var foregroundColor = new BrushConverter().ConvertFromString("#ffffff") as SolidColorBrush;
                var inputColor = new BrushConverter().ConvertFromString("#1a1a1a") as SolidColorBrush;
                var borderBrush = new BrushConverter().ConvertFromString("#f5f5f5") as SolidColorBrush;

                back.Background = backBackgroundColor;
                btnInputPanel.Background = backBackgroundColor;

                btnOk.Background = btnBackgroundColor;
                btnOk.Foreground = foregroundColor;
                btnOtmena.Background = btnBackgroundColor;
                btnOtmena.Foreground = foregroundColor;

                InputName.Background = inputColor;
                InputBirthDate.Background = inputColor;
                InputDeathDate.Background = inputColor;

                InputName.BorderBrush = borderBrush;
                InputBirthDate.BorderBrush = borderBrush;
                InputDeathDate.BorderBrush = borderBrush;

                InputName.Foreground = foregroundColor;
                InputBirthDate.Foreground = foregroundColor;
                InputDeathDate.Foreground = foregroundColor;

                Name.Foreground = foregroundColor;
                BirthDate.Foreground = foregroundColor;
                DeathDate.Foreground = foregroundColor;
            }
            else
            {
                var backBackgroundColor = new BrushConverter().ConvertFromString("#f5f5f5") as SolidColorBrush;
                var btnBackgroundColor = new BrushConverter().ConvertFromString("#b0b0b0") as SolidColorBrush;
                var foregroundColor = new BrushConverter().ConvertFromString("#000000") as SolidColorBrush;
                var inputColor = new BrushConverter().ConvertFromString("#ffffff") as SolidColorBrush;
                var borderBrush = new BrushConverter().ConvertFromString("#1a1a1a") as SolidColorBrush;

                back.Background = backBackgroundColor;
                btnInputPanel.Background = backBackgroundColor;

                btnOk.Background = btnBackgroundColor;
                btnOk.Foreground = foregroundColor;
                btnOtmena.Background = btnBackgroundColor;
                btnOtmena.Foreground = foregroundColor;

                InputName.Background = inputColor;
                InputBirthDate.Background = inputColor;
                InputDeathDate.Background = inputColor;

                InputName.BorderBrush = borderBrush;
                InputBirthDate.BorderBrush = borderBrush;
                InputDeathDate.BorderBrush = borderBrush;
                
                InputName.Foreground = foregroundColor;
                InputBirthDate.Foreground = foregroundColor;
                InputDeathDate.Foreground = foregroundColor;

                Name.Foreground = foregroundColor;
                BirthDate.Foreground = foregroundColor;
                DeathDate.Foreground = foregroundColor;
            }
        }

        private void Accept_Click(object sender, RoutedEventArgs e)
        {
            string name = InputName.Text;
            string birth = InputBirthDate.Text;
            string death = InputDeathDate.Text;
            
            if (CheckInputData.CheckCorrectlyInput(name,birth,death))
            {
                _newPerson = new Person(name, DateHelper.ParseDate(birth),
                    death != string.Empty ? DateHelper.ParseDate(death) : null);
                InputName.Text = string.Empty;
                InputBirthDate.Text = string.Empty;
                InputDeathDate.Text = string.Empty;
                Hide();
            }
        }

        public Person GetPerson() => _newPerson;
        public bool CheckPersonExist() => _newPerson != null;

        public void SetNullPerson() => _newPerson = null;
        
        private void ButtonBase_OnClick(object sender, RoutedEventArgs e) => Hide();
    }
}