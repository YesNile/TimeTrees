using System.Drawing;
using System.Windows;
using TimeTrees.DesktopGui.States;
using System.Windows.Media;
using TimeTrees.DesktopGui.Model;

namespace TimeTrees.DesktopGui.Windows
{
    public partial class ViewRectangleInfo : Window
    {
        private readonly Theme theme;

        public ViewRectangleInfo(Theme theme)
        {
            this.theme = theme;
            InitializeComponent();
            SetThemeEditInfo(theme);
        }

        private void SetThemeEditInfo(Theme currentTheme)
        {
            if (currentTheme == Theme.Black)
            {
                var backBackgroundColor = new BrushConverter().ConvertFromString("#3d3d3d") as SolidColorBrush;
                var btnBackgroundColor = new BrushConverter().ConvertFromString("#2e2e2e") as SolidColorBrush;
                var foregroundColor = new BrushConverter().ConvertFromString("#ffffff") as SolidColorBrush;
                var boxesBackgroundColor = new BrushConverter().ConvertFromString("#1a1a1a") as SolidColorBrush;
                var borderBrush = new BrushConverter().ConvertFromString("#f5f5f5") as SolidColorBrush;

                RectangleColour.Background = backBackgroundColor;
                btnPanel.Background = backBackgroundColor;
                btnEdit.Background = btnBackgroundColor;
                btnClose.Background = btnBackgroundColor;

                btnEdit.Foreground = foregroundColor;
                btnClose.Foreground = foregroundColor;

                IdInfo.Foreground = foregroundColor;
                NameInfo.Foreground = foregroundColor;
                BirthInfo.Foreground = foregroundColor;
                DeathInfo.Foreground = foregroundColor;

                IdBox.Background = boxesBackgroundColor;
                NameBox.Background = boxesBackgroundColor;
                BirthBox.Background = boxesBackgroundColor;
                DeathBox.Background = boxesBackgroundColor;

                IdBox.Foreground = foregroundColor;
                NameBox.Foreground = foregroundColor;
                BirthBox.Foreground = foregroundColor;
                DeathBox.Foreground = foregroundColor;

                IdBorder.BorderBrush = borderBrush;
                NameBorder.BorderBrush = borderBrush;
                BirthBorder.BorderBrush = borderBrush;
                DeathBorder.BorderBrush = borderBrush;
            }
            else
            {
                var backBackgroundColor = new BrushConverter().ConvertFromString("#f5f5f5") as SolidColorBrush;
                var btnBackgroundColor = new BrushConverter().ConvertFromString("#b0b0b0") as SolidColorBrush;
                var foregroundColor = new BrushConverter().ConvertFromString("#000000") as SolidColorBrush;
                var boxesBackgroundColor = new BrushConverter().ConvertFromString("#ffffff") as SolidColorBrush;
                var borderBrush = new BrushConverter().ConvertFromString("#1a1a1a") as SolidColorBrush;

                RectangleColour.Background = backBackgroundColor;
                btnPanel.Background = backBackgroundColor;
                btnEdit.Background = btnBackgroundColor;
                btnClose.Background = btnBackgroundColor;

                btnEdit.Foreground = foregroundColor;
                btnClose.Foreground = foregroundColor;

                IdInfo.Foreground = foregroundColor;
                NameInfo.Foreground = foregroundColor;
                BirthInfo.Foreground = foregroundColor;
                DeathInfo.Foreground = foregroundColor;

                IdBox.Background = boxesBackgroundColor;
                NameBox.Background = boxesBackgroundColor;
                BirthBox.Background = boxesBackgroundColor;
                DeathBox.Background = boxesBackgroundColor;

                IdBox.Foreground = foregroundColor;
                NameBox.Foreground = foregroundColor;
                BirthBox.Foreground = foregroundColor;
                DeathBox.Foreground = foregroundColor;

                IdBorder.BorderBrush = borderBrush;
                NameBorder.BorderBrush = borderBrush;
                BirthBorder.BorderBrush = borderBrush;
                DeathBorder.BorderBrush = borderBrush;
            }
        }

        private void BtnClose_OnClick(object sender, RoutedEventArgs e) => Close();

        private void BtnEdit_OnClick(object sender, RoutedEventArgs e)
        {
            EditInfo editInfo = new EditInfo(theme);
            editInfo.DataContext = this.DataContext;
            Hide();
            editInfo.ShowDialog();
            ShowDialog();
        }

    }
}