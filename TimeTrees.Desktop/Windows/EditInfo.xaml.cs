using System.Windows;
using System.Windows.Shapes;
using TimeTrees.Core.Models;
using TimeTrees.Core.WorkWithDateTools;
using TimeTrees.DesktopGui.Additional_tools;
using TimeTrees.DesktopGui.DAO;
using TimeTrees.DesktopGui.Model;
using TimeTrees.DesktopGui.States;
using System.Windows.Media;

namespace TimeTrees.DesktopGui.Windows
{
    public partial class EditInfo : Window
    {
        public EditInfo(Theme theme)
        {
            InitializeComponent();
            SetThemeEditInfo(theme);
        }

        private void SetThemeEditInfo(Theme currentTheme)
        {
            if (currentTheme == Theme.Black)
            {
                var backBackgroundColor = new BrushConverter().ConvertFromString("#3d3d3d") as SolidColorBrush;
                var btnBackgroundColor = new BrushConverter().ConvertFromString("#2e2e2e") as SolidColorBrush;
                var foregroundColor = Brushes.White;
                var editColor = new BrushConverter().ConvertFromString("#1a1a1a") as SolidColorBrush;
                var borderBrush = new BrushConverter().ConvertFromString("#f5f5f5") as SolidColorBrush;
                
                InfoBack.Background = backBackgroundColor;
                StackPanel.Background = backBackgroundColor;

                btnSave.Background = btnBackgroundColor;
                btnSave.Foreground = foregroundColor;
                btnClose.Background = btnBackgroundColor;
                btnClose.Foreground = foregroundColor;

                IdBlock.Foreground = foregroundColor;
                NameBlock.Foreground = foregroundColor;
                BirthBlock.Foreground = foregroundColor;
                DeathBlock.Foreground = foregroundColor;

                IdEdit.Background = editColor;
                EditName.Background = editColor;
                EditBirth.Background = editColor;
                EditDeath.Background = editColor;

                IdBrush.BorderBrush = borderBrush;
                EditName.BorderBrush = borderBrush;
                EditBirth.BorderBrush = borderBrush;
                EditDeath.BorderBrush = borderBrush;

                IdEdit.Foreground = foregroundColor;
                EditName.Foreground = foregroundColor;
                EditBirth.Foreground = foregroundColor;
                EditDeath.Foreground = foregroundColor;

            }
            else
            {
                var backBackgroundColor = new BrushConverter().ConvertFromString("#f5f5f5") as SolidColorBrush;
                var btnBackgroundColor = new BrushConverter().ConvertFromString("#b0b0b0") as SolidColorBrush;
                var foregroundColor = Brushes.Black;
                var editColor = new BrushConverter().ConvertFromString("#ffffff") as SolidColorBrush;
                var borderBrush = new BrushConverter().ConvertFromString("#1a1a1a") as SolidColorBrush;
                
                InfoBack.Background = backBackgroundColor;
                StackPanel.Background = backBackgroundColor;

                btnSave.Background = btnBackgroundColor;
                btnSave.Foreground = foregroundColor;
                btnClose.Background = btnBackgroundColor;
                btnClose.Foreground = foregroundColor;

                IdBlock.Foreground = foregroundColor;
                NameBlock.Foreground = foregroundColor;
                BirthBlock.Foreground = foregroundColor;
                DeathBlock.Foreground = foregroundColor;

                IdEdit.Background = editColor;
                EditName.Background = editColor;
                EditBirth.Background = editColor;
                EditDeath.Background = editColor;

                IdBrush.BorderBrush = borderBrush;
                EditName.BorderBrush = borderBrush;
                EditBirth.BorderBrush = borderBrush;
                EditDeath.BorderBrush = borderBrush;

                IdEdit.Foreground = foregroundColor;
                EditName.Foreground = foregroundColor;
                EditBirth.Foreground = foregroundColor;
                EditDeath.Foreground = foregroundColor;
            }
        }

        private void BtnClose_OnClick(object sender, RoutedEventArgs e) => Close();

        private void BtnEdit_OnClick(object sender, RoutedEventArgs e)
        {
            string name = EditName.Text;
            string birthDate = EditBirth.Text;
            string deathDate = EditDeath.Text;
            
            if (DataContext is Rectangle rectangle)
            {
                if (rectangle.DataContext is Person person)
                {
                    if (CheckInputData.CheckCorrectlyInput(name, birthDate, deathDate))
                    {
                        person = new Person(person.Id, name, DateHelper.ParseDate(birthDate), deathDate != string.Empty ? DateHelper.ParseDate(deathDate) : null);
                        rectangle.DataContext = person;
                        Close();
                        RectangleDao rectangleDao = new RectangleDao();
                        rectangleDao.Update(new RectangleModel(rectangle));
                        MessageBox.Show("Отредактировано!");
                    }
                }
            }
        }
    }
}