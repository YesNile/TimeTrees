using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using TimeTrees.DesktopGui.States;

namespace TimeTrees.DesktopGui.Windows
{
    public partial class ConnectTypeChoice
    {
        private static string _choice;
        public ConnectTypeChoice(Theme theme)
        {
            InitializeComponent();
            SetThemeConnect(theme);
        }

        private void SetThemeConnect(Theme currentTheme)
        {
            if (currentTheme == Theme.Black)
            {
                var panelBackgroundColor = new BrushConverter().ConvertFromString("#3d3d3d") as SolidColorBrush;
                var btnBackgroundColor = new BrushConverter().ConvertFromString("#2e2e2e") as SolidColorBrush;
                var radioColor = Brushes.White;
                var foregroundColor = Brushes.White;

                ConnectionGrid.Background = panelBackgroundColor;
                bntConfirm.Background = btnBackgroundColor;
                bntCloseType.Background = btnBackgroundColor;
                bntConfirm.Foreground = foregroundColor;
                bntCloseType.Foreground = foregroundColor;
                Parent.Foreground = radioColor;
                Spouses.Foreground = radioColor;
                Info.Background = panelBackgroundColor;
                Info.Foreground = foregroundColor;

            }
            else
            {
                var panelBackgroundColor = new BrushConverter().ConvertFromString("#f5f5f5") as SolidColorBrush;
                var btnBackgroundColor = new BrushConverter().ConvertFromString("#b0b0b0") as SolidColorBrush;
                var radioColor = Brushes.Black;
                var foregroundColor = Brushes.Black;

                ConnectionGrid.Background = panelBackgroundColor;
                bntConfirm.Background = btnBackgroundColor;
                bntCloseType.Background = btnBackgroundColor;
                bntConfirm.Foreground = foregroundColor;
                bntCloseType.Foreground = foregroundColor;
                Parent.Foreground = radioColor;
                Spouses.Foreground = radioColor;
                Info.Background = panelBackgroundColor;
                Info.Foreground = foregroundColor;

            }
        }
        private void BtnClose_OnClick(object sender, RoutedEventArgs e) => Close();

        private void RadioButton_Checked(object sender, RoutedEventArgs e)
        {
            _choice = ((RadioButton) sender).Content.ToString();
        }

        public string GetChoice() => _choice;

        private void ButtonBase_OnClick(object sender, RoutedEventArgs e) => Hide();
    }
}