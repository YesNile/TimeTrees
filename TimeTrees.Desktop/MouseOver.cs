﻿using System.Collections.Generic;
using System.Windows.Controls;
using System.Windows.Shapes;

namespace TimeTrees.Desktop;

public class MouseOver
{
    private readonly Canvas _canvas;

    public MouseOver(Canvas canvas)
    {
        _canvas = canvas;
    }

    public List<Shape> GetMouseOver()
    {
        List<Shape> selectedItems = new List<Shape>();
        foreach (var child in _canvas.Children)
        {
            if (child is Shape {IsMouseOver: true} shape)
            {
                selectedItems.Add(shape);
            }
                
        }

        return selectedItems;
    }
}