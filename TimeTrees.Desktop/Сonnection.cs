﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;

namespace TimeTrees.Desktop;

public class Сonnection
{
    public Polyline Polyline = new Polyline() {Stroke = Brushes.Black};

    Rectangle[] Rectangles = new Rectangle [2];

    public void AddRect(Rectangle rectangle)
    {
        if (Rectangles[0] is null)
            Rectangles[0] = rectangle;
        else Rectangles[1] = rectangle;
    }

    public void Update()
    {
        Polyline.Points.Clear();
        Polyline.Points.Add(new Point(Canvas.GetLeft(Rectangles[0])+ Rectangles[0].Width/2, Canvas.GetTop(Rectangles[0]) + Rectangles[0].Height/2));
        Polyline.Points.Add(new Point(Canvas.GetLeft(Rectangles[1]) + Rectangles[1].Width/2, Canvas.GetTop(Rectangles[1]) + Rectangles[0].Height/2));
    }

    public void Update(Point point)
    {
        Polyline.Points.Clear();
        Polyline.Points.Add(new Point(Canvas.GetLeft(Rectangles[0])+ Rectangles[0].Width/2, Canvas.GetTop(Rectangles[0]) + Rectangles[0].Height/2));
        Polyline.Points.Add(point);
    }

    public void CheckAndUpdate(Rectangle rectangle)
    {
        if (Rectangles[0] == rectangle || Rectangles[1] == rectangle)
            Update();
    }
}