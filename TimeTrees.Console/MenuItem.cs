using TimeTrees.ConsoleUI;

namespace TimeTrees
{
    public class MenuItem
    {
        public ICommand Execute { get; set; }
        public string Description { get; set; }
        public bool Select { get; set; }
    }
}