using TimeTrees.Core.ReadTools;

namespace TimeTrees.ConsoleUI
{
    public class FormatMenuItem : MenuItem
    {
        public Format Format { get; set; }
    }
}