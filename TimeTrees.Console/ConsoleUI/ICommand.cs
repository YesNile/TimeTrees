namespace TimeTrees.ConsoleUI
{
    public interface ICommand
    {
        void Execute();
    }
}