using System;
using TimeTrees.Core;
using TimeTrees.Core.Dao;
using TimeTrees.Core.Models;
using TimeTrees.Core.WorkWithDateTools;
using TimeTrees.Core.WriteTools;

namespace TimeTrees.ConsoleUI
{
    public class AddPersonLogic : ICommand
    {
        public void Execute()
        {
            Console.CursorVisible = true;
            Console.Clear();
            Console.WriteLine("Добавление нового человека");
            NewPerson();
            Console.CursorVisible = false;
            if (ConsoleHelper.Continue()) Execute();
        }

        static void NewPerson()
        {
            PersonDao personDao = Program.PersonDao;
            
            Person person = new Person();
            
            Console.Write("Ведите имя человека или нажмите Enter для выхода: ");
            person.Name = Console.ReadLine();
            if (string.IsNullOrEmpty(person.Name)) return;
            
            Console.Write("Введите точную дату рождения человека (год-месяц-день): ");
            person.BirthDate = DateHelper.CheckBirthDateCorectly(Console.ReadLine());
            
            Console.Write("Введите точную дату смерти человека (если она есть): ");
            person.DeathDate = DateHelper.CheckDeathDateCorectly(Console.ReadLine(), person.BirthDate);
            
            Console.WriteLine("Нажмите Enter, если у человека неизвестны родители или кнопку F, чтобы найти родственников: ");
            ConsoleKeyInfo key;
            do
            {
                key = Console.ReadKey();
                if (key.Key != ConsoleKey.Enter && key.Key != ConsoleKey.F)
                {
                    Console.WriteLine("Неизвестная клавиша. Введите ENTER или F.");
                }
            } while (key.Key != ConsoleKey.Enter && key.Key != ConsoleKey.F);

            if (key.Key == ConsoleKey.F)
            {
                person.FirstParent = CheckParentBirthDateCorectly(person);
                ConsoleHelper.ClearScreen();
                Console.Write("Хотите ввести еще родителя? [y/n]");
                ConsoleKeyInfo answer = Console.ReadKey();
                if (answer.Key == ConsoleKey.Y)
                {
                    person.SecondParent = CheckParentBirthDateCorectly(person);
                }
                ConsoleHelper.ClearScreen();
            }
            
            personDao.Add(person);
            WriteData.WritePeopleInFile(personDao.GetAll(), Program.Format);
            Console.WriteLine("Данные успешно добавлены");
        }

        public static Person CheckParentBirthDateCorectly(Person person)
        {
            Person findPerson = FindInListLogic.FindInList();
            do
            {
                try
                {
                    if (!Program.PersonDao.CheckCorrectlyAgeParent(person.Id, findPerson.Id))
                        throw new Exception("Дата рождения родителя не может быть меньше даты рождения ребенка");
                    break;
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                    Console.WriteLine("Хотите закончить поиск? [y/n]");
                    ConsoleKeyInfo key = Console.ReadKey();
                    if(key.Key == ConsoleKey.N)
                        findPerson = FindInListLogic.FindInList();
                    else if(key.Key == ConsoleKey.Y)
                        break;
                }
                    
            } while (true);

            return findPerson;
        }
    }
}