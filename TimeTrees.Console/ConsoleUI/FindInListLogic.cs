using System;
using System.Collections.Generic;
using System.Linq;
using TimeTrees.Core.Dao;
using TimeTrees.Core.Models;

namespace TimeTrees.ConsoleUI
{
    public class FindInListLogic
    {
        public static Person FindInList()
        {
            string name = string.Empty;
            Console.CursorVisible = true;
            int selectedItemId = 0;
            List<Person> found;
            bool exit = false;
            PersonDao personDao = Program.PersonDao;
            var personList = personDao.GetAll();
            Person person = new Person();
            do
            { 
                ConsoleHelper.ClearScreen();
                Console.WriteLine(name);
                found = FilterList(personList, name);
                
                ShowList(found,selectedItemId);
                
                Console.SetCursorPosition($"{name}".Length, 0);
                ConsoleKeyInfo keyInfo = Console.ReadKey();

                
                if (char.IsLetter(keyInfo.KeyChar) || char.IsNumber(keyInfo.KeyChar))
                {
                    name += keyInfo.KeyChar;
                    selectedItemId = default;
                }
                else
                {
                    switch (keyInfo.Key)
                    {
                        case ConsoleKey.DownArrow:
                            selectedItemId = selectedItemId == found.Count
                                ? 1
                                : selectedItemId + 1;
                            break;
                        case ConsoleKey.UpArrow:
                            selectedItemId = selectedItemId == 0 
                                ? found.Count
                                : selectedItemId == 1
                                    ? found.Count
                                    : selectedItemId - 1;
                            break;
                        case ConsoleKey.Enter:
                            Console.SetCursorPosition(0,found.Count + 2);
                            if (ConsoleHelper.ConfirmAction())
                            { 
                                person = found[selectedItemId - 1];
                                exit = true;
                            }
                            break;
                        case ConsoleKey.Escape:
                            Console.SetCursorPosition(0,found.Count + 2);
                            if (ConsoleHelper.ConfirmAction())
                            {
                                person = null;
                                exit = true;
                            }
                            break;
                        case ConsoleKey.Backspace:
                            if (name.Length != 0)
                            {
                                name = name.Remove(name.Length - 1);
                                selectedItemId = default;
                            }
                            break;
                    }
                }
            } while (!exit);

            Console.CursorVisible = false;
            return person;
        }
        
        static void ShowList(List<Person> list, int select)
        {
            foreach (var person in list)
            {
                Console.BackgroundColor = select == list.IndexOf(person) + 1
                    ? ConsoleColor.DarkCyan
                    : ConsoleColor.Black;
                Console.WriteLine($"{person.Id} \t{person.Name} \t{person.BirthDate.ToShortDateString()} \t{person.DeathDate}");
            }
        }

        static List<Person> FilterList(List<Person> personList, string value) => personList.Where(x => x.Name.Contains(value, StringComparison.OrdinalIgnoreCase)).ToList();
    }
}