using System;

namespace TimeTrees
{
    public class ConsoleHelper
    {
        public static void ClearScreen()
        {
            Console.BackgroundColor = ConsoleColor.Black;
            for (int i = 0; i < Console.WindowHeight; i++)
            {
                Console.SetCursorPosition(0, i);
                Console.Write(new String(' ', Console.WindowWidth));
            }

            Console.SetCursorPosition(0, 0);
        }
        
        public static bool Continue()
        {
            do
            {
                Console.WriteLine("Хотите выйти? [Y/N]");
                ConsoleKeyInfo keyInfo = Console.ReadKey(true);
                if (keyInfo.Key == ConsoleKey.Y | keyInfo.Key == ConsoleKey.N)
                {
                    return !(keyInfo.Key == ConsoleKey.Y);
                }
                else
                {
                    Console.WriteLine("Неизвестная клавиша");
                }
            } while (true);
        }
        public static bool ConfirmAction()
        {
            // ClearScreen();
            Console.WriteLine("Для подтверждения выбора нажмите Enter или Esc для отмены");
            do
            {
                ConsoleKeyInfo keyInfo = Console.ReadKey();
                if (keyInfo.Key == ConsoleKey.Enter | keyInfo.Key == ConsoleKey.Escape)
                {
                    return keyInfo.Key == ConsoleKey.Enter;
                }
            } while (true);
        }
    }
}